// Code generated by protoc-gen-go. DO NOT EDIT.
// source: headphone/headphone.proto

package protocols

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type PlayRequest struct {
	MediaID              string   `protobuf:"bytes,1,opt,name=mediaID" json:"mediaID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PlayRequest) Reset()         { *m = PlayRequest{} }
func (m *PlayRequest) String() string { return proto.CompactTextString(m) }
func (*PlayRequest) ProtoMessage()    {}
func (*PlayRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{0}
}
func (m *PlayRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PlayRequest.Unmarshal(m, b)
}
func (m *PlayRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PlayRequest.Marshal(b, m, deterministic)
}
func (dst *PlayRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PlayRequest.Merge(dst, src)
}
func (m *PlayRequest) XXX_Size() int {
	return xxx_messageInfo_PlayRequest.Size(m)
}
func (m *PlayRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PlayRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PlayRequest proto.InternalMessageInfo

func (m *PlayRequest) GetMediaID() string {
	if m != nil {
		return m.MediaID
	}
	return ""
}

type AudioBulk struct {
	Data                 []byte   `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AudioBulk) Reset()         { *m = AudioBulk{} }
func (m *AudioBulk) String() string { return proto.CompactTextString(m) }
func (*AudioBulk) ProtoMessage()    {}
func (*AudioBulk) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{1}
}
func (m *AudioBulk) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AudioBulk.Unmarshal(m, b)
}
func (m *AudioBulk) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AudioBulk.Marshal(b, m, deterministic)
}
func (dst *AudioBulk) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AudioBulk.Merge(dst, src)
}
func (m *AudioBulk) XXX_Size() int {
	return xxx_messageInfo_AudioBulk.Size(m)
}
func (m *AudioBulk) XXX_DiscardUnknown() {
	xxx_messageInfo_AudioBulk.DiscardUnknown(m)
}

var xxx_messageInfo_AudioBulk proto.InternalMessageInfo

func (m *AudioBulk) GetData() []byte {
	if m != nil {
		return m.Data
	}
	return nil
}

type AudioDetails struct {
	Title                string   `protobuf:"bytes,1,opt,name=title" json:"title,omitempty"`
	Id                   string   `protobuf:"bytes,2,opt,name=id" json:"id,omitempty"`
	ArtistName           string   `protobuf:"bytes,5,opt,name=artistName" json:"artistName,omitempty"`
	CoverURL             string   `protobuf:"bytes,6,opt,name=coverURL" json:"coverURL,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AudioDetails) Reset()         { *m = AudioDetails{} }
func (m *AudioDetails) String() string { return proto.CompactTextString(m) }
func (*AudioDetails) ProtoMessage()    {}
func (*AudioDetails) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{2}
}
func (m *AudioDetails) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AudioDetails.Unmarshal(m, b)
}
func (m *AudioDetails) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AudioDetails.Marshal(b, m, deterministic)
}
func (dst *AudioDetails) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AudioDetails.Merge(dst, src)
}
func (m *AudioDetails) XXX_Size() int {
	return xxx_messageInfo_AudioDetails.Size(m)
}
func (m *AudioDetails) XXX_DiscardUnknown() {
	xxx_messageInfo_AudioDetails.DiscardUnknown(m)
}

var xxx_messageInfo_AudioDetails proto.InternalMessageInfo

func (m *AudioDetails) GetTitle() string {
	if m != nil {
		return m.Title
	}
	return ""
}

func (m *AudioDetails) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *AudioDetails) GetArtistName() string {
	if m != nil {
		return m.ArtistName
	}
	return ""
}

func (m *AudioDetails) GetCoverURL() string {
	if m != nil {
		return m.CoverURL
	}
	return ""
}

type GetMainPageListRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetMainPageListRequest) Reset()         { *m = GetMainPageListRequest{} }
func (m *GetMainPageListRequest) String() string { return proto.CompactTextString(m) }
func (*GetMainPageListRequest) ProtoMessage()    {}
func (*GetMainPageListRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{3}
}
func (m *GetMainPageListRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMainPageListRequest.Unmarshal(m, b)
}
func (m *GetMainPageListRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMainPageListRequest.Marshal(b, m, deterministic)
}
func (dst *GetMainPageListRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMainPageListRequest.Merge(dst, src)
}
func (m *GetMainPageListRequest) XXX_Size() int {
	return xxx_messageInfo_GetMainPageListRequest.Size(m)
}
func (m *GetMainPageListRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMainPageListRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetMainPageListRequest proto.InternalMessageInfo

type GetMainPageListResponse struct {
	AudiosDetails        []*AudioDetails `protobuf:"bytes,1,rep,name=audiosDetails" json:"audiosDetails,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *GetMainPageListResponse) Reset()         { *m = GetMainPageListResponse{} }
func (m *GetMainPageListResponse) String() string { return proto.CompactTextString(m) }
func (*GetMainPageListResponse) ProtoMessage()    {}
func (*GetMainPageListResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{4}
}
func (m *GetMainPageListResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMainPageListResponse.Unmarshal(m, b)
}
func (m *GetMainPageListResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMainPageListResponse.Marshal(b, m, deterministic)
}
func (dst *GetMainPageListResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMainPageListResponse.Merge(dst, src)
}
func (m *GetMainPageListResponse) XXX_Size() int {
	return xxx_messageInfo_GetMainPageListResponse.Size(m)
}
func (m *GetMainPageListResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMainPageListResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetMainPageListResponse proto.InternalMessageInfo

func (m *GetMainPageListResponse) GetAudiosDetails() []*AudioDetails {
	if m != nil {
		return m.AudiosDetails
	}
	return nil
}

type AddAudioRequest struct {
	// Types that are valid to be assigned to RequestSelector:
	//	*AddAudioRequest_Bulk
	//	*AddAudioRequest_Details
	RequestSelector      isAddAudioRequest_RequestSelector `protobuf_oneof:"requestSelector"`
	XXX_NoUnkeyedLiteral struct{}                          `json:"-"`
	XXX_unrecognized     []byte                            `json:"-"`
	XXX_sizecache        int32                             `json:"-"`
}

func (m *AddAudioRequest) Reset()         { *m = AddAudioRequest{} }
func (m *AddAudioRequest) String() string { return proto.CompactTextString(m) }
func (*AddAudioRequest) ProtoMessage()    {}
func (*AddAudioRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{5}
}
func (m *AddAudioRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AddAudioRequest.Unmarshal(m, b)
}
func (m *AddAudioRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AddAudioRequest.Marshal(b, m, deterministic)
}
func (dst *AddAudioRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AddAudioRequest.Merge(dst, src)
}
func (m *AddAudioRequest) XXX_Size() int {
	return xxx_messageInfo_AddAudioRequest.Size(m)
}
func (m *AddAudioRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_AddAudioRequest.DiscardUnknown(m)
}

var xxx_messageInfo_AddAudioRequest proto.InternalMessageInfo

type isAddAudioRequest_RequestSelector interface {
	isAddAudioRequest_RequestSelector()
}

type AddAudioRequest_Bulk struct {
	Bulk *AudioBulk `protobuf:"bytes,1,opt,name=bulk,oneof"`
}
type AddAudioRequest_Details struct {
	Details *AudioDetails `protobuf:"bytes,2,opt,name=details,oneof"`
}

func (*AddAudioRequest_Bulk) isAddAudioRequest_RequestSelector()    {}
func (*AddAudioRequest_Details) isAddAudioRequest_RequestSelector() {}

func (m *AddAudioRequest) GetRequestSelector() isAddAudioRequest_RequestSelector {
	if m != nil {
		return m.RequestSelector
	}
	return nil
}

func (m *AddAudioRequest) GetBulk() *AudioBulk {
	if x, ok := m.GetRequestSelector().(*AddAudioRequest_Bulk); ok {
		return x.Bulk
	}
	return nil
}

func (m *AddAudioRequest) GetDetails() *AudioDetails {
	if x, ok := m.GetRequestSelector().(*AddAudioRequest_Details); ok {
		return x.Details
	}
	return nil
}

// XXX_OneofFuncs is for the internal use of the proto package.
func (*AddAudioRequest) XXX_OneofFuncs() (func(msg proto.Message, b *proto.Buffer) error, func(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error), func(msg proto.Message) (n int), []interface{}) {
	return _AddAudioRequest_OneofMarshaler, _AddAudioRequest_OneofUnmarshaler, _AddAudioRequest_OneofSizer, []interface{}{
		(*AddAudioRequest_Bulk)(nil),
		(*AddAudioRequest_Details)(nil),
	}
}

func _AddAudioRequest_OneofMarshaler(msg proto.Message, b *proto.Buffer) error {
	m := msg.(*AddAudioRequest)
	// requestSelector
	switch x := m.RequestSelector.(type) {
	case *AddAudioRequest_Bulk:
		b.EncodeVarint(1<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Bulk); err != nil {
			return err
		}
	case *AddAudioRequest_Details:
		b.EncodeVarint(2<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Details); err != nil {
			return err
		}
	case nil:
	default:
		return fmt.Errorf("AddAudioRequest.RequestSelector has unexpected type %T", x)
	}
	return nil
}

func _AddAudioRequest_OneofUnmarshaler(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error) {
	m := msg.(*AddAudioRequest)
	switch tag {
	case 1: // requestSelector.bulk
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(AudioBulk)
		err := b.DecodeMessage(msg)
		m.RequestSelector = &AddAudioRequest_Bulk{msg}
		return true, err
	case 2: // requestSelector.details
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(AudioDetails)
		err := b.DecodeMessage(msg)
		m.RequestSelector = &AddAudioRequest_Details{msg}
		return true, err
	default:
		return false, nil
	}
}

func _AddAudioRequest_OneofSizer(msg proto.Message) (n int) {
	m := msg.(*AddAudioRequest)
	// requestSelector
	switch x := m.RequestSelector.(type) {
	case *AddAudioRequest_Bulk:
		s := proto.Size(x.Bulk)
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(s))
		n += s
	case *AddAudioRequest_Details:
		s := proto.Size(x.Details)
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(s))
		n += s
	case nil:
	default:
		panic(fmt.Sprintf("proto: unexpected type %T in oneof", x))
	}
	return n
}

type AddAudioResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AddAudioResponse) Reset()         { *m = AddAudioResponse{} }
func (m *AddAudioResponse) String() string { return proto.CompactTextString(m) }
func (*AddAudioResponse) ProtoMessage()    {}
func (*AddAudioResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_headphone_c6ae44bf1af34b31, []int{6}
}
func (m *AddAudioResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AddAudioResponse.Unmarshal(m, b)
}
func (m *AddAudioResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AddAudioResponse.Marshal(b, m, deterministic)
}
func (dst *AddAudioResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AddAudioResponse.Merge(dst, src)
}
func (m *AddAudioResponse) XXX_Size() int {
	return xxx_messageInfo_AddAudioResponse.Size(m)
}
func (m *AddAudioResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_AddAudioResponse.DiscardUnknown(m)
}

var xxx_messageInfo_AddAudioResponse proto.InternalMessageInfo

func init() {
	proto.RegisterType((*PlayRequest)(nil), "protocols.PlayRequest")
	proto.RegisterType((*AudioBulk)(nil), "protocols.AudioBulk")
	proto.RegisterType((*AudioDetails)(nil), "protocols.AudioDetails")
	proto.RegisterType((*GetMainPageListRequest)(nil), "protocols.GetMainPageListRequest")
	proto.RegisterType((*GetMainPageListResponse)(nil), "protocols.GetMainPageListResponse")
	proto.RegisterType((*AddAudioRequest)(nil), "protocols.AddAudioRequest")
	proto.RegisterType((*AddAudioResponse)(nil), "protocols.AddAudioResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// HeadphoneServiceClient is the client API for HeadphoneService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type HeadphoneServiceClient interface {
	GetMainPageList(ctx context.Context, in *GetMainPageListRequest, opts ...grpc.CallOption) (*GetMainPageListResponse, error)
	AddAudio(ctx context.Context, opts ...grpc.CallOption) (HeadphoneService_AddAudioClient, error)
}

type headphoneServiceClient struct {
	cc *grpc.ClientConn
}

func NewHeadphoneServiceClient(cc *grpc.ClientConn) HeadphoneServiceClient {
	return &headphoneServiceClient{cc}
}

func (c *headphoneServiceClient) GetMainPageList(ctx context.Context, in *GetMainPageListRequest, opts ...grpc.CallOption) (*GetMainPageListResponse, error) {
	out := new(GetMainPageListResponse)
	err := c.cc.Invoke(ctx, "/protocols.HeadphoneService/GetMainPageList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *headphoneServiceClient) AddAudio(ctx context.Context, opts ...grpc.CallOption) (HeadphoneService_AddAudioClient, error) {
	stream, err := c.cc.NewStream(ctx, &_HeadphoneService_serviceDesc.Streams[0], "/protocols.HeadphoneService/AddAudio", opts...)
	if err != nil {
		return nil, err
	}
	x := &headphoneServiceAddAudioClient{stream}
	return x, nil
}

type HeadphoneService_AddAudioClient interface {
	Send(*AddAudioRequest) error
	CloseAndRecv() (*AddAudioResponse, error)
	grpc.ClientStream
}

type headphoneServiceAddAudioClient struct {
	grpc.ClientStream
}

func (x *headphoneServiceAddAudioClient) Send(m *AddAudioRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *headphoneServiceAddAudioClient) CloseAndRecv() (*AddAudioResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(AddAudioResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// HeadphoneServiceServer is the server API for HeadphoneService service.
type HeadphoneServiceServer interface {
	GetMainPageList(context.Context, *GetMainPageListRequest) (*GetMainPageListResponse, error)
	AddAudio(HeadphoneService_AddAudioServer) error
}

func RegisterHeadphoneServiceServer(s *grpc.Server, srv HeadphoneServiceServer) {
	s.RegisterService(&_HeadphoneService_serviceDesc, srv)
}

func _HeadphoneService_GetMainPageList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMainPageListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeadphoneServiceServer).GetMainPageList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protocols.HeadphoneService/GetMainPageList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeadphoneServiceServer).GetMainPageList(ctx, req.(*GetMainPageListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HeadphoneService_AddAudio_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(HeadphoneServiceServer).AddAudio(&headphoneServiceAddAudioServer{stream})
}

type HeadphoneService_AddAudioServer interface {
	SendAndClose(*AddAudioResponse) error
	Recv() (*AddAudioRequest, error)
	grpc.ServerStream
}

type headphoneServiceAddAudioServer struct {
	grpc.ServerStream
}

func (x *headphoneServiceAddAudioServer) SendAndClose(m *AddAudioResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *headphoneServiceAddAudioServer) Recv() (*AddAudioRequest, error) {
	m := new(AddAudioRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _HeadphoneService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "protocols.HeadphoneService",
	HandlerType: (*HeadphoneServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetMainPageList",
			Handler:    _HeadphoneService_GetMainPageList_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "AddAudio",
			Handler:       _HeadphoneService_AddAudio_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "headphone/headphone.proto",
}

func init() {
	proto.RegisterFile("headphone/headphone.proto", fileDescriptor_headphone_c6ae44bf1af34b31)
}

var fileDescriptor_headphone_c6ae44bf1af34b31 = []byte{
	// 368 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x91, 0xcf, 0x6e, 0xda, 0x40,
	0x10, 0xc6, 0xb1, 0xcb, 0xdf, 0x81, 0x16, 0xba, 0x42, 0xc5, 0x75, 0xa5, 0x96, 0xee, 0xa5, 0xa8,
	0x07, 0x2a, 0xc1, 0xb9, 0x07, 0x10, 0x55, 0x89, 0x44, 0x22, 0x64, 0x14, 0x89, 0xeb, 0xe2, 0x1d,
	0x85, 0x15, 0x86, 0x75, 0xbc, 0x6b, 0xa4, 0x3c, 0x40, 0x9e, 0x28, 0x2f, 0x18, 0xb1, 0xb6, 0x89,
	0x21, 0x24, 0x27, 0xef, 0xcc, 0xfc, 0x34, 0xf3, 0x7d, 0x9f, 0xe1, 0xeb, 0x1a, 0x19, 0x0f, 0xd7,
	0x72, 0x87, 0x7f, 0x8e, 0xaf, 0x7e, 0x18, 0x49, 0x2d, 0x49, 0xcd, 0x7c, 0x7c, 0x19, 0x28, 0xfa,
	0x0b, 0xea, 0xf3, 0x80, 0x3d, 0x78, 0x78, 0x1f, 0xa3, 0xd2, 0xc4, 0x81, 0xca, 0x16, 0xb9, 0x60,
	0x57, 0x13, 0xc7, 0xea, 0x5a, 0xbd, 0x9a, 0x97, 0x95, 0xf4, 0x07, 0xd4, 0x46, 0x31, 0x17, 0x72,
	0x1c, 0x07, 0x1b, 0x42, 0xa0, 0xc8, 0x99, 0x66, 0x86, 0x69, 0x78, 0xe6, 0x4d, 0x43, 0x68, 0x18,
	0x60, 0x82, 0x9a, 0x89, 0x40, 0x91, 0x36, 0x94, 0xb4, 0xd0, 0x01, 0xa6, 0x8b, 0x92, 0x82, 0x7c,
	0x02, 0x5b, 0x70, 0xc7, 0x36, 0x2d, 0x5b, 0x70, 0xf2, 0x1d, 0x80, 0x45, 0x5a, 0x28, 0x7d, 0xc3,
	0xb6, 0xe8, 0x94, 0x4c, 0x3f, 0xd7, 0x21, 0x2e, 0x54, 0x7d, 0xb9, 0xc7, 0xe8, 0xd6, 0x9b, 0x39,
	0x65, 0x33, 0x3d, 0xd6, 0xd4, 0x81, 0x2f, 0xff, 0x51, 0x5f, 0x33, 0xb1, 0x9b, 0xb3, 0x3b, 0x9c,
	0x09, 0xa5, 0x53, 0x1b, 0x74, 0x09, 0x9d, 0x57, 0x13, 0x15, 0xca, 0x9d, 0x42, 0xf2, 0x17, 0x3e,
	0xb2, 0x83, 0x4c, 0x95, 0xea, 0x74, 0xac, 0xee, 0x87, 0x5e, 0x7d, 0xd0, 0xe9, 0x1f, 0x33, 0xe9,
	0xe7, 0x6d, 0x78, 0xa7, 0x34, 0x7d, 0xb4, 0xa0, 0x39, 0xe2, 0xdc, 0x20, 0x59, 0x68, 0xbf, 0xa1,
	0xb8, 0x8a, 0x83, 0x8d, 0x31, 0x5a, 0x1f, 0xb4, 0xcf, 0x37, 0x1d, 0x12, 0x9b, 0x16, 0x3c, 0xc3,
	0x90, 0x21, 0x54, 0x78, 0x7a, 0xd8, 0x36, 0xf8, 0x5b, 0x87, 0xa7, 0x05, 0x2f, 0x23, 0xc7, 0x9f,
	0xa1, 0x19, 0x25, 0xb7, 0x16, 0x18, 0xa0, 0xaf, 0x65, 0x44, 0x09, 0xb4, 0x5e, 0x64, 0x24, 0xd6,
	0x06, 0x4f, 0x16, 0xb4, 0xa6, 0xd9, 0xaf, 0x5e, 0x60, 0xb4, 0x17, 0x3e, 0x92, 0x25, 0x34, 0xcf,
	0xa2, 0x20, 0x3f, 0x73, 0x27, 0x2f, 0x07, 0xe8, 0xd2, 0xf7, 0x90, 0x34, 0xc9, 0x7f, 0x50, 0xcd,
	0x24, 0x10, 0x37, 0xef, 0xe2, 0x34, 0x1e, 0xf7, 0xdb, 0xc5, 0x59, 0xb2, 0xa4, 0x67, 0xad, 0xca,
	0x66, 0x3a, 0x7c, 0x0e, 0x00, 0x00, 0xff, 0xff, 0x20, 0x55, 0x25, 0xc3, 0xb0, 0x02, 0x00, 0x00,
}
