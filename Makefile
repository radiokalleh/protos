ROOT := gitlab.com/radiokalleh/protos
SERVICES = $(filter-out tools/ proto_cpp/ proto_py/ proto_go/, $(wildcard */))
PROTO_SRCS = $(wildcard $(SERVICES)*.proto)
PROTO_GOS = $(patsubst %/%.proto,proto_go/%.pb.go,$(PROTO_SRCS))
PROTO_CPPS = $(patsubst %/%.proto,proto_cpp/%.pb.cc,$(PROTO_SRCS))
PROTO_PY3S = $(patsubst %/%.proto,proto_py/%_pb3.py,$(PROTO_SRCS))

all: proto_gen

go-dependencies:
	$(GO_VARS) $(GO) get -u github.com/golang/protobuf/protoc-gen-go github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	make $(PROTO_GOS)
	$(GO_VARS) $(GO) list -f='{{ join .Deps "\n" }}' $(ROOT)/proto_go | grep -v $(ROOT) | tr '\n' ' ' | $(GO_VARS) xargs $(GO) get -u
	$(GO_VARS) $(GO) list -f='{{ join .Deps "\n" }}' $(ROOT)/proto_go | grep -v $(ROOT) | tr '\n' ' ' | $(GO_VARS) xargs $(GO) install

cpp-dependencies:
	cd tools && git clone https://github.com/grpc/grpc -b `curl -L https://grpc.io/release`
	cd tools/grpc && git submodule update --init
	cd tools/grpc && make && sudo make install

py-dependencies:
	$(GO_VARS) $(GO) get -u github.com/golang/protobuf/protoc-gen-go github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	make $(PROTO_PY3S)

clean:
	rm -rf proto_cpp/$(SERVICES) proto_go/$(SERVICES) proto_py/$(SERVICES)

tools/protoc/bin/protoc:
	mkdir -p tools/protoc/
	curl -L -o tools/protoc.zip https://github.com/google/protobuf/releases/download/v3.5.1/protoc-3.5.1-$(OS)-$(ARCH).zip
	unzip tools/protoc.zip -d tools/protoc/
	rm tools/protoc.zip

tools/protoc/bin/grpc_python_plugin:
	mkdir -p tools/grpc_src/
	git clone --depth 1 https://github.com/grpc/grpc.git tools/grpc_src
	git -C tools/grpc_src submodule update --init
	make -C tools/grpc_src/ grpc_python_plugin
	cp tools/grpc_src/bins/opt/grpc_python_plugin tools/protoc/bin/grpc_python_plugin
	rm -rf tools/grpc_src/

${PROTO_SRCS}:
	tools/protoc/bin/protoc -Itools/protoc/include -I. \
		-I${GOPATH}/src -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--python_out=plugins=grpc:proto_py \
		--plugin=protoc-gen-grpc_python=tools/protoc/bin/grpc_python_plugin \
		--grpc_python_out=proto_py $@

	tools/protoc/bin/protoc -Itools/protoc/include -I. \
		-I${GOPATH}/src -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:proto_go $@

	tools/protoc/bin/protoc -Itools/protoc/include -I. \
		--grpc_out=proto_cpp --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` \
	    --cpp_out=proto_cpp $@

proto_cpp/$(SERVICES)version.generated.cc: $(PROTO_CPPS) ${PROTO_SRCS} proto_cpp/scripts/*.go
	cd $(patsubst %/version.generated.cc,%,$@); COMMIT=$(COMMIT) VERSION=$(VERSION) BUILD_TIME=$(BUILD_TIME) BUILDER_ID=$(BUILDER_ID) go run ../scripts/writeversion.go

proto_go/$(SERVICES)version.generated.go: $(PROTO_GOS) ${PROTO_SRCS} proto_go/scripts/*.go
	cd $(patsubst %/version.generated.go,%,$@); COMMIT=$(COMMIT) VERSION=$(VERSION) BUILD_TIME=$(BUILD_TIME) BUILDER_ID=$(BUILDER_ID) go run ../scripts/writeversion.go

proto_py/$(SERVICES)version_generated.py: $(PROTO_PY3S) ${PROTO_SRCS} proto_py/scripts/*.go
	cd $(patsubst %/version_generated.py,%,$@); COMMIT=$(COMMIT) VERSION=$(VERSION) BUILD_TIME=$(BUILD_TIME) BUILDER_ID=$(BUILDER_ID) go run ../scripts/writeversion.go

proto_gen: proto_go/$(SERVICES)version.generated.go \
		proto_py/$(SERVICES)version_generated.py \
		proto_cpp/$(SERVICES)version.generated.cc

.PHONY: clean proto_gen go-dependencies cpp-dependencies all clean ${PROTO_SRCS}

## Commons Vars ##########################################################
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
	OS ?= linux
	GOOS ?= linux
endif
ifeq ($(UNAME_S), Darwin)
	OS ?= osx
	GOOS ?= darwin
endif
ARCH := $(shell uname -m)
ifeq ($(ARCH), unknown)
	GOARCH ?= amd64
	ARCH := x86_64
endif
ifeq ($(ARCH), x86_64)
	GOARCH ?= amd64
endif
ifeq ($(ARCH), i386)
	ARCH = x86_32
	GOARCH ?= 386
endif

GO ?= go
GIT ?= git
COMMIT := $(shell $(GIT) rev-parse HEAD)
PREV_COMMIT := $(shell $(GIT) rev-parse HEAD~)
VERSION ?= $(shell $(GIT) describe --tags ${COMMIT} 2> /dev/null || echo "$(COMMIT)")
BUILD_TIME := $(shell LANG=en_US date +"%F_%T_%z")
BUILDER_ID := $(shell LANG=en_US id -un)@$(shell LANG=en_US hostname -f)
GO_VARS = GOOS=$(GOOS) GOARCH=$(GOARCH)
