# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: headphone/headphone.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='headphone/headphone.proto',
  package='protocols',
  syntax='proto3',
  serialized_pb=_b('\n\x19headphone/headphone.proto\x12\tprotocols\"\x1e\n\x0bPlayRequest\x12\x0f\n\x07mediaID\x18\x01 \x01(\t\"\x19\n\tAudioBulk\x12\x0c\n\x04\x64\x61ta\x18\x01 \x01(\x0c\"O\n\x0c\x41udioDetails\x12\r\n\x05title\x18\x01 \x01(\t\x12\n\n\x02id\x18\x02 \x01(\t\x12\x12\n\nartistName\x18\x05 \x01(\t\x12\x10\n\x08\x63overURL\x18\x06 \x01(\t\"\x18\n\x16GetMainPageListRequest\"I\n\x17GetMainPageListResponse\x12.\n\raudiosDetails\x18\x01 \x03(\x0b\x32\x17.protocols.AudioDetails\"v\n\x0f\x41\x64\x64\x41udioRequest\x12$\n\x04\x62ulk\x18\x01 \x01(\x0b\x32\x14.protocols.AudioBulkH\x00\x12*\n\x07\x64\x65tails\x18\x02 \x01(\x0b\x32\x17.protocols.AudioDetailsH\x00\x42\x11\n\x0frequestSelector\"\x12\n\x10\x41\x64\x64\x41udioResponse2\xb3\x01\n\x10HeadphoneService\x12X\n\x0fGetMainPageList\x12!.protocols.GetMainPageListRequest\x1a\".protocols.GetMainPageListResponse\x12\x45\n\x08\x41\x64\x64\x41udio\x12\x1a.protocols.AddAudioRequest\x1a\x1b.protocols.AddAudioResponse(\x01\x62\x06proto3')
)




_PLAYREQUEST = _descriptor.Descriptor(
  name='PlayRequest',
  full_name='protocols.PlayRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='mediaID', full_name='protocols.PlayRequest.mediaID', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=40,
  serialized_end=70,
)


_AUDIOBULK = _descriptor.Descriptor(
  name='AudioBulk',
  full_name='protocols.AudioBulk',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='data', full_name='protocols.AudioBulk.data', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=97,
)


_AUDIODETAILS = _descriptor.Descriptor(
  name='AudioDetails',
  full_name='protocols.AudioDetails',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='title', full_name='protocols.AudioDetails.title', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='id', full_name='protocols.AudioDetails.id', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='artistName', full_name='protocols.AudioDetails.artistName', index=2,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='coverURL', full_name='protocols.AudioDetails.coverURL', index=3,
      number=6, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=99,
  serialized_end=178,
)


_GETMAINPAGELISTREQUEST = _descriptor.Descriptor(
  name='GetMainPageListRequest',
  full_name='protocols.GetMainPageListRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=180,
  serialized_end=204,
)


_GETMAINPAGELISTRESPONSE = _descriptor.Descriptor(
  name='GetMainPageListResponse',
  full_name='protocols.GetMainPageListResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='audiosDetails', full_name='protocols.GetMainPageListResponse.audiosDetails', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=206,
  serialized_end=279,
)


_ADDAUDIOREQUEST = _descriptor.Descriptor(
  name='AddAudioRequest',
  full_name='protocols.AddAudioRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='bulk', full_name='protocols.AddAudioRequest.bulk', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='details', full_name='protocols.AddAudioRequest.details', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='requestSelector', full_name='protocols.AddAudioRequest.requestSelector',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=281,
  serialized_end=399,
)


_ADDAUDIORESPONSE = _descriptor.Descriptor(
  name='AddAudioResponse',
  full_name='protocols.AddAudioResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=401,
  serialized_end=419,
)

_GETMAINPAGELISTRESPONSE.fields_by_name['audiosDetails'].message_type = _AUDIODETAILS
_ADDAUDIOREQUEST.fields_by_name['bulk'].message_type = _AUDIOBULK
_ADDAUDIOREQUEST.fields_by_name['details'].message_type = _AUDIODETAILS
_ADDAUDIOREQUEST.oneofs_by_name['requestSelector'].fields.append(
  _ADDAUDIOREQUEST.fields_by_name['bulk'])
_ADDAUDIOREQUEST.fields_by_name['bulk'].containing_oneof = _ADDAUDIOREQUEST.oneofs_by_name['requestSelector']
_ADDAUDIOREQUEST.oneofs_by_name['requestSelector'].fields.append(
  _ADDAUDIOREQUEST.fields_by_name['details'])
_ADDAUDIOREQUEST.fields_by_name['details'].containing_oneof = _ADDAUDIOREQUEST.oneofs_by_name['requestSelector']
DESCRIPTOR.message_types_by_name['PlayRequest'] = _PLAYREQUEST
DESCRIPTOR.message_types_by_name['AudioBulk'] = _AUDIOBULK
DESCRIPTOR.message_types_by_name['AudioDetails'] = _AUDIODETAILS
DESCRIPTOR.message_types_by_name['GetMainPageListRequest'] = _GETMAINPAGELISTREQUEST
DESCRIPTOR.message_types_by_name['GetMainPageListResponse'] = _GETMAINPAGELISTRESPONSE
DESCRIPTOR.message_types_by_name['AddAudioRequest'] = _ADDAUDIOREQUEST
DESCRIPTOR.message_types_by_name['AddAudioResponse'] = _ADDAUDIORESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

PlayRequest = _reflection.GeneratedProtocolMessageType('PlayRequest', (_message.Message,), dict(
  DESCRIPTOR = _PLAYREQUEST,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.PlayRequest)
  ))
_sym_db.RegisterMessage(PlayRequest)

AudioBulk = _reflection.GeneratedProtocolMessageType('AudioBulk', (_message.Message,), dict(
  DESCRIPTOR = _AUDIOBULK,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.AudioBulk)
  ))
_sym_db.RegisterMessage(AudioBulk)

AudioDetails = _reflection.GeneratedProtocolMessageType('AudioDetails', (_message.Message,), dict(
  DESCRIPTOR = _AUDIODETAILS,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.AudioDetails)
  ))
_sym_db.RegisterMessage(AudioDetails)

GetMainPageListRequest = _reflection.GeneratedProtocolMessageType('GetMainPageListRequest', (_message.Message,), dict(
  DESCRIPTOR = _GETMAINPAGELISTREQUEST,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.GetMainPageListRequest)
  ))
_sym_db.RegisterMessage(GetMainPageListRequest)

GetMainPageListResponse = _reflection.GeneratedProtocolMessageType('GetMainPageListResponse', (_message.Message,), dict(
  DESCRIPTOR = _GETMAINPAGELISTRESPONSE,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.GetMainPageListResponse)
  ))
_sym_db.RegisterMessage(GetMainPageListResponse)

AddAudioRequest = _reflection.GeneratedProtocolMessageType('AddAudioRequest', (_message.Message,), dict(
  DESCRIPTOR = _ADDAUDIOREQUEST,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.AddAudioRequest)
  ))
_sym_db.RegisterMessage(AddAudioRequest)

AddAudioResponse = _reflection.GeneratedProtocolMessageType('AddAudioResponse', (_message.Message,), dict(
  DESCRIPTOR = _ADDAUDIORESPONSE,
  __module__ = 'headphone.headphone_pb2'
  # @@protoc_insertion_point(class_scope:protocols.AddAudioResponse)
  ))
_sym_db.RegisterMessage(AddAudioResponse)



_HEADPHONESERVICE = _descriptor.ServiceDescriptor(
  name='HeadphoneService',
  full_name='protocols.HeadphoneService',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=422,
  serialized_end=601,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetMainPageList',
    full_name='protocols.HeadphoneService.GetMainPageList',
    index=0,
    containing_service=None,
    input_type=_GETMAINPAGELISTREQUEST,
    output_type=_GETMAINPAGELISTRESPONSE,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='AddAudio',
    full_name='protocols.HeadphoneService.AddAudio',
    index=1,
    containing_service=None,
    input_type=_ADDAUDIOREQUEST,
    output_type=_ADDAUDIORESPONSE,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_HEADPHONESERVICE)

DESCRIPTOR.services_by_name['HeadphoneService'] = _HEADPHONESERVICE

# @@protoc_insertion_point(module_scope)
