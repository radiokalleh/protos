from distutils.core import setup
from setuptools import find_packages

setup(
    name='noise-protos',
    version='1.0',
    packages=find_packages(),
    url='https://gitlab.com/radiokalleh/protos',
    author='Arya Hadi',
    author_email='arya.hadi97@gmail.com',
)
